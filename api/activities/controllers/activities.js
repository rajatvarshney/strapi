'use strict';
const nodemailer = require("nodemailer");
/**
 * Read the documentation (https://strapi.io/documentation/v3.x/concepts/controllers.html#core-controllers)
 * to customize this controller
 */

module.exports = {
    updatePrice: async ctx => {
        
        // extracting discount from request body
        const { Discount } = ctx.request.body;
        
        // fetching all activitities from database
        const result = await strapi
            .query('activities')
            .model.fetchAll()
            .then(function (resData) {
                
                // loop through each activity
                resData.models.forEach(function (model) {
                    
                    var recordId = model.attributes.id;
                    var currentPrice = model.attributes.price;
                    
                    // calculating discounted price
                    var discountAmount = (Discount / 100) * currentPrice;
                    var discountPrint = currentPrice - discountAmount;

                    // updating activity with discounted price 
                    strapi.query('activities').update(
                        { id: recordId },
                        {
                            price: discountPrint,

                        }
                    );
                });
            });

        return "success";

    },
    sendNotifcation: async ctx => {

        let testAccount = await nodemailer.createTestAccount();
        // create reusable transporter object using the default SMTP transport
        let transporter = nodemailer.createTransport({
            host: "smtp.ethereal.email",
            port: 587,
            secure: false, // true for 465, false for other ports
            auth: {
                user: testAccount.user, // generated ethereal user
                pass: testAccount.pass, // generated ethereal password
            },
        });

        // send mail with defined transport object
        let info = await transporter.sendMail({
            from: '"Fred Foo" <sender@gmail.com>', // sender address
            to: "info@mallorcard.es", // list of receivers
            subject: "Hello Mallor card", // Subject line
            text: "New Activity Addedd...", // plain text body
            html: "<b>New Activity Addedd.../b>", // html body
        });

        return "success";

    }

}
